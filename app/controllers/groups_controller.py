from flask import request, jsonify, current_app
from sqlalchemy.exc import IntegrityError
from app.exceptions import GroupNameError, InvalidFieldTypeError, WrongFieldsError
from app.models.groups_model import GroupModel
from app.models.users_model import UserModel
from app.models.character_model import CharacterModel
from app.models.groups_users import groups_users
from psycopg2.errors import UniqueViolation
from flask_jwt_extended import jwt_required, get_jwt_identity


@jwt_required()
def create_group():
    user = UserModel.query.get_or_404(get_jwt_identity().get('id'))
    data = request.get_json()
    session = current_app.db.session
    try:
        GroupModel.verify_fields(data)
        new_group = GroupModel(**data)
        user.grupos.append(new_group)
        session.add(user)
        session.add(new_group)
        session.commit()
    except TypeError:
        return {"error": "Invalid fields"}, 400
    except IntegrityError as e:
        if type(e.orig) == UniqueViolation:
            return {"error": "Group already exists"}, 409
        return {"error": "User do not exists"}, 404
    except (InvalidFieldTypeError,GroupNameError,WrongFieldsError) as e:
        return e.message, 400
    return jsonify(new_group), 201


def get_groups():
    result = current_app.db.session.query(GroupModel).select_from(GroupModel).join(groups_users).join(UserModel).all()

    return jsonify([{
        "id": group.id,
        "name": group.name,
        "master": group.master_id,
        "members":
        [user for user in group.members]
    } for group in result]
    ), 200


def get_one_group(group_id):
    result = current_app.db.session.query(GroupModel).select_from(GroupModel).join(groups_users).join(UserModel).filter(GroupModel.id == group_id).all()
    if not result:
        return {"error": "Group not found "}, 404
    return jsonify([{
        "id": group.id,
        "name": group.name,
        "master": group.master_id,
        "members":
        [user for user in group.members]
    } for group in result]
    ), 200


@jwt_required()
def att_group(group_id):
    session = current_app.db.session
    data = request.get_json()
    try:
        group = GroupModel.query.filter_by(id=group_id).first_or_404()
        GroupModel.verify_fields(data)
        user = UserModel.query.get(get_jwt_identity().get('id'))
        if user not in group.members:
            if user.id != group.master_id:
                return {"message": "Must be part of the group"}, 401
        for key, value in data.items():
            setattr(group, key, value)
        session.add(group)
        session.commit()
        return jsonify(group)
    except IntegrityError:
        return {"error": "Cannot change id field"}, 400
    except (InvalidFieldTypeError,GroupNameError,WrongFieldsError) as e:
        return e.message, 400


def remove_user_character(group_id):
    session = current_app.db.session
    if request.args.get('user'):
        user = UserModel.query.get(request.args.get('user'))
        group = GroupModel.query.get_or_404(group_id)
        if group not in user.grupos:
            return {"error": "User not in the group"}, 409
        group.members.remove(user)
        session.commit()
        return {"message": f"{user.name} foi removido do grupo {group.name}"}
    if request.args.get('char'):
        group = GroupModel.query.filter_by(id=group_id).first_or_404()
        char = CharacterModel.query.get_or_404(request.args.get('char'))
        if group not in char.grupos:
            return {"error": "Character not in the group"}
        session = current_app.db.session
        group.characters.remove(char)
        session.commit()
        return {"message": f"{char.name} foi removido do grupo {group.name}"}


@jwt_required()
def del_group(group_id):
    if not request.args.get('user') and not request.args.get('char'):
        user = get_jwt_identity()
        group = GroupModel.query.filter_by(id=group_id).first_or_404()
        if user['email'] in [item.email for item in  group.members]:
            session = current_app.db.session
            session.delete(group)
            session.commit()
            return {"message": f"Grupo '{group.name}' deletado com sucesso"}, 200
        return {"error": "must be part of the group"}, 401
    return remove_user_character(group_id)


@jwt_required()
def add_user_character(group_id):
    if request.args.get('user'):
        session = current_app.db.session
        user = UserModel.query.get_or_404(request.args.get('user'))
        group = GroupModel.query.get_or_404(group_id)
        if group in user.grupos:
            return {"error": "User already in the group"}, 409
        user.grupos.append(group)
        session.add(user)
        session.commit()

        return {"message": f"{user.name} entrou no grupo {group.name}"}, 200
    if request.args.get('char'):
        session = current_app.db.session
        char = CharacterModel.query.get_or_404(
            request.args.get('char'))
        group = GroupModel.query.get_or_404(group_id)
        if char.user_id not in [item.id for item in group.members]:
            return {"error": "character's owner must be in the group"}, 400
        if char in group.characters:
            return {"error": "Character already in the group"}, 409
        char.grupos.append(group)
        session.commit()
        return {"message": f"{char.name} adicionado ao grupo {group.name}"}, 200
