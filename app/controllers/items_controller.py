from flask import request, jsonify, current_app
from flask_jwt_extended import jwt_required
from werkzeug.exceptions import NotFound
from app.models.items_model import ItemModel
from app.models.character_model import CharacterModel
from app.models.type_model import TypeModel
from app.exceptions import (
    NoRequisitionBodyError,
    WrongFieldsError,
    NotAcceptedFieldError,
    CharIdNeededError,
    CharacterNotFoundError,
    InvalidFieldTypeError,
    TypeNotFoundError
)


@jwt_required()
def create_item(char_id: int):
    session = current_app.db.session
    data = request.get_json()
    
    try:  
        if not data:
            raise NoRequisitionBodyError

        char = CharacterModel.query.get(char_id)
        if not char:
            raise CharacterNotFoundError

        ItemModel.verify_fields_create(data)
    
        new_item = ItemModel(**data)
        char.items.append(new_item)

        session.add(new_item)
        session.commit()
        

        return {
                "id": new_item.id,
                "name": new_item.name,
                "weight": new_item.weight,
                "description": new_item.description,
                "damage": new_item.damage,
                "max_ammo": new_item.max_ammo,
                "ammo": new_item.ammo,
                "attacks": new_item.attacks,
                "range": new_item.range,
                "defect": new_item.defect,
                "area": new_item.area,
                "type": new_item.type.name
            }, 201

    except (NoRequisitionBodyError, WrongFieldsError, InvalidFieldTypeError) as err:
        return jsonify(err.message), 400

    except (CharacterNotFoundError, TypeNotFoundError) as err:
        return jsonify(err.message), 404


@jwt_required()
def update_item(item_id: int):
    session = current_app.db.session
    data = request.get_json()
    
    try:
        if not data:
            raise NoRequisitionBodyError

        ItemModel.verify_fields_update(data)

        item = ItemModel.query.get_or_404(item_id)

        for key, value in data.items():
            setattr(item, key, value)

        session.add(item)
        session.commit()

        return {
            "id": item.id,
            "name": item.name,
            "weight": item.weight,
            "description": item.description,
            "damage": item.damage,
            "max_ammo": item.max_ammo,
            "ammo": item.ammo,
            "attacks": item.attacks,
            "range": item.range,
            "defect": item.defect,
            "area": item.area,
            "type": item.type.name
        }, 200

    except NotFound:
        return jsonify({'error': 'item not found'}), 404

    except (NoRequisitionBodyError, InvalidFieldTypeError, NotAcceptedFieldError) as err:
        return jsonify(err.message), 400

    except TypeNotFoundError as err:
        return jsonify(err.message), 404


@jwt_required()
def delete_item(item_id: int):
    session = current_app.db.session

    try:
        char_id = request.args.get('char_id')
        if not char_id:
            raise CharIdNeededError

        char = CharacterModel.query.get(char_id)
        if not char:
            raise CharacterNotFoundError

        item = ItemModel.query.get_or_404(item_id)

        char.items.remove(item) 
        session.delete(item)
        session.commit()

        return jsonify({'message': f'character "{item.name}" successfully deleted'}), 200

    except NotFound:
        return jsonify({'error': 'item not found'}), 404

    except CharIdNeededError as err:
        return jsonify(err.message), 400

    except CharacterNotFoundError as err:
        return jsonify(err.message), 404


@jwt_required()
def get_all_types():
    types =  TypeModel.query.all()

    return jsonify([
            {
                "id": type.id,
                "name": type.name
            } for type in types
        ]), 200
