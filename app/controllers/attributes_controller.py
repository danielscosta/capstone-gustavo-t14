from flask import current_app, request, jsonify
from flask_jwt_extended import jwt_required, get_jwt_identity
from sqlalchemy import and_
from app.controllers import validate_access
from app.models.attributes_model import AttributeModel
from app.models.character_model import CharacterModel
from werkzeug.exceptions import NotFound
from app.exceptions import (
    NoRequisitionBodyError,
    InvalidFieldTypeError,
    NotAcceptedFieldError,
)


@jwt_required()
@validate_access
def update_attributes(char_id):
    session = current_app.db.session

    data = request.get_json()

    try:
        if not data:
            raise NoRequisitionBodyError

        AttributeModel.verify_fields_update(data)

        user = get_jwt_identity()

        character = CharacterModel.query.filter(and_(CharacterModel.user_id == user['id'], CharacterModel.id == char_id)).first_or_404()

        for key, value in data.items():
            setattr(character.attributes, key, value)

        session.commit()

        return jsonify({'character': character.name, 'attributes': character.attributes}), 200

    except NotFound:
        return jsonify({'error': 'character not found'}), 404

    except (NoRequisitionBodyError, NotAcceptedFieldError, InvalidFieldTypeError) as err:
        return jsonify(err.message), 400
