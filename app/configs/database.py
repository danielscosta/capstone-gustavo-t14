from flask import Flask
from flask_sqlalchemy import SQLAlchemy


db = SQLAlchemy()


def init_app(app: Flask) -> None:
    db.init_app(app)
    app.db = db

    from app.models.users_model import UserModel
    from app.models.character_model import CharacterModel
    from app.models.attributes_model import AttributeModel
    from app.models.skills_model import SkillModel
    from app.models.items_model import ItemModel
    from app.models.type_model import TypeModel
    from app.models.groups_model import GroupModel
    from app.models.characters_skills_model import CharactersSkills
    from app.models.characters_items import character_items
    from app.models.groups_users import groups_users
    from app.models.groups_characters import groups_characters
