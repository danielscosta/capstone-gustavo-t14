from flask import Flask
from flask_cors import CORS
from app.configs import env_configs, database, migrations, auth
from app import routes


def create_app() -> Flask:
    app = Flask(__name__)

    CORS(app)

    env_configs.init_app(app)
    database.init_app(app)
    migrations.init_app(app)
    auth.init_app(app)
    routes.init_app(app)

    return app
