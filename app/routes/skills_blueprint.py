from flask import Blueprint
from app.controllers.skills_controller import create_skill, update_skill, delete_skill


bp = Blueprint('bp_skills', __name__, url_prefix='/skill')


bp.post('/<int:char_id>')(create_skill)
bp.patch('/<int:skill_id>')(update_skill)
bp.delete('/<int:skill_id>')(delete_skill)
