from flask import Blueprint
from app.controllers.character_controller import (
    create_character,
    read_all_characters,
    read_character,
    update_character,
    delete_character
)


bp = Blueprint('bp_characters', __name__, url_prefix='/character')


bp.post('')(create_character)
bp.get('')(read_all_characters)
bp.get('<int:char_id>')(read_character)
bp.patch('<int:char_id>')(update_character)
bp.delete('<int:char_id>')(delete_character)
