from flask import Blueprint
from app.controllers.users_controller import (
    signup,
    login,
    read_user,
    update_user,
    delete_user
)


bp = Blueprint('bp_users', __name__, url_prefix='/user')


bp.post('/signup')(signup)
bp.post('/login')(login)
bp.get('')(read_user)
bp.patch('')(update_user)
bp.delete('')(delete_user)
