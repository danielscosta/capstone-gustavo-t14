from flask import Blueprint
from app.controllers.groups_controller import (
    add_user_character,
    att_group,
    create_group,
    del_group,
    get_groups,
    get_one_group,
    remove_user_character
)


bp = Blueprint('bp_groups', __name__, url_prefix='/group')


bp.post('')(create_group)
bp.get('')(get_groups)
bp.get('/<int:group_id>')(get_one_group)
bp.patch('/<int:group_id>')(att_group)
bp.delete('/<int:group_id>')(del_group)
bp.post('/<int:group_id>')(add_user_character)
bp.delete('/<int:group_id>')(remove_user_character)
