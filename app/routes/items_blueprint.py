from flask import Blueprint
from app.controllers.items_controller import (
    create_item,
    delete_item, 
    get_all_types,
    update_item
)


bp = Blueprint('bp_items', __name__, url_prefix='/item')


bp.post('/<int:char_id>')(create_item)
bp.get('/types')(get_all_types)
bp.patch('/<int:item_id>')(update_item)
bp.delete('/<int:item_id>')(delete_item)
