from flask import Blueprint
from app.controllers.home_controller import home


bp = Blueprint('bp_home', __name__)


bp.get('/')(home)
