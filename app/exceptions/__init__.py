class NoRequisitionBodyError(Exception):
    def __init__(self) -> None:
        self.message = {'error': 'missing requisition body'}


class WrongFieldsError(Exception):
    def __init__(self, missing_fields: list, unsolicited_fields) -> None:
        self.message = {
            'error': 'wrong fields',
            'missing fields': missing_fields,
            'unsolicited fields': unsolicited_fields
        }


class NotAcceptedFieldError(Exception):
    def __init__(self, received_field: str, accepted_fields: list) -> None:
        self.message = {
            'error': f'field \'{received_field}\' not accepted',
            'accepted fields': accepted_fields
        }


class InvalidFieldTypeError(Exception):
    def __init__(self, key: str, value: any, must_be: any) -> None:
        types = {
            str: 'string',
            int: 'integer',
            float: 'float',
            list: 'list',
            dict: 'dictionary',
            bool: 'boolean',
        }

        self.message = {
            'error': 'invalid field type',
            'invalid field': {
                key: types[type(value)],
                'must be': must_be
            }
        }


class LoginError(Exception):
    def __init__(self) -> None:
        self.message = {'error': 'email and password mismatch'}


class CharIdNeededError(Exception):
    def __init__(self):
        self.message = {'error': 'query param needed: char_id'}


class CharacterNotFoundError(Exception):
    def __init__(self):
        self.message = {'error': "Character not found!"}

class GroupNameError(Exception):
    def __init__(self):
        self.message = {'error': "group name must be at least 3 characters"}

class TypeNotFoundError(Exception):
    def __init__(self):
        self.message = {'error': 'Type not found!'}