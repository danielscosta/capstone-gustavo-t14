from app.configs.database import db
from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy.orm import relationship, validates
from dataclasses import dataclass

from app.exceptions import GroupNameError, InvalidFieldTypeError, WrongFieldsError


@dataclass
class GroupModel(db.Model):
    id: int
    name: str
    master_id: int

    __tablename__ = 'groups'

    id = Column(Integer, primary_key=True)
    name = Column(String(32), nullable=False, unique=True)
    master_id = Column(Integer, ForeignKey('users.id'))

    members = relationship('UserModel', backref='grupos', secondary='groups_users', cascade_backrefs=True)
    
    characters = relationship('CharacterModel', backref='grupos', secondary='groups_characters', cascade_backrefs=True)
    @validates('name', 'master_id')
    def validate_fields(self, key: str, value: any) -> any:
        if key  == 'name':
            if type(value) is not str:
                raise InvalidFieldTypeError(key, value, 'String')
            if len(value) < 3:
                raise GroupNameError
            else:
                return value
        if key == 'master_id':
            if type(value) is not int:
                raise InvalidFieldTypeError(key, value, 'Integer')
            else:
                return value
        return value

    @staticmethod
    def verify_fields(request: dict) -> None:
        required_fields = ['name']
        not_required_fields = ['master_id']

        if list(request.keys()) != required_fields:
            missing_fields = [field for field in required_fields if field not in request.keys()]
            unsolicited_fields = [field for field in request.keys() if field not in required_fields and field not in not_required_fields]
            if missing_fields or unsolicited_fields:
                raise WrongFieldsError(missing_fields, unsolicited_fields)