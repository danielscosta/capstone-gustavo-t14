from app.configs.database import db
from sqlalchemy import Column, Integer
from sqlalchemy.orm import validates
from dataclasses import dataclass
from app.exceptions import InvalidFieldTypeError, NotAcceptedFieldError


@dataclass
class AttributeModel(db.Model):
    constitution: int
    power: int
    strength: int
    dexterity: int
    intelligence: int
    luck: int
    appearance: int
    education: int

    __tablename__ = 'attributes'

    id = Column(Integer, primary_key=True)
    constitution = Column(Integer, nullable=False, default=0)
    power = Column(Integer, nullable=False, default=0)
    strength = Column(Integer, nullable=False, default=0)
    dexterity = Column(Integer, nullable=False, default=0)
    intelligence = Column(Integer, nullable=False, default=0)
    luck = Column(Integer, nullable=False, default=0)
    appearance = Column(Integer, nullable=False, default=0)
    education = Column(Integer, nullable=False, default=0)


    @validates('constitution', 'power', 'strength', 'dexterity', 'intelligence', 'luck', 'appearance', 'education')
    def validate_fields(self, key: str, value: any) -> int:
        if type(value) != int:
            raise InvalidFieldTypeError(key, value, 'integer')
        
        return value


    @staticmethod
    def create_attributes(session) -> int:
        attributes = {
            'constitution': 0,
            'power': 0,
            'strength': 0,
            'dexterity': 0,
            'intelligence': 0,
            'luck': 0,
            'appearance': 0,
            'education': 0
        }

        new_attribute = AttributeModel(**attributes)

        session.add(new_attribute)
        session.commit()

        return new_attribute.id


    @staticmethod
    def delete_attributes(session, id: int) -> None:
        deleted_attributes = AttributeModel.query.get(id)
        
        session.delete(deleted_attributes)
        session.commit()


    @staticmethod
    def verify_fields_update(request: dict) -> None:
        accepted_fields = ['constitution', 'power', 'strength', 'dexterity', 'intelligence', 'luck', 'appearance', 'education']

        for field in request.keys():
            if field not in accepted_fields:
                raise NotAcceptedFieldError(field, accepted_fields)
