from app.configs.database import db
from sqlalchemy import Column, Integer, ForeignKey


groups_characters = db.Table('groups_characters',
    Column('id', Integer, primary_key=True),
    Column('character_id', Integer, ForeignKey("characters.id"), nullable=False),
    Column('group_id', Integer, ForeignKey('groups.id'), nullable=False))
