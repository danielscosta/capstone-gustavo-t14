from app.configs.database import db
from sqlalchemy import Column, String, Integer
from dataclasses import dataclass
from app.exceptions import InvalidFieldTypeError, WrongFieldsError, NotAcceptedFieldError


@dataclass
class SkillModel(db.Model):
    id: int
    name: str

    __tablename__ = 'skills'

    id = Column(Integer, primary_key=True)
    name = Column(String(16), nullable=False)


    @staticmethod
    def verify_fields_create(request: dict) -> None:
        required_fields = ['name']
        not_required_fields = []

        if list(request.keys()) != required_fields:
            missing_fields = [field for field in required_fields if field not in request.keys()]
            unsolicited_fields = [field for field in request.keys() if field not in required_fields and field not in not_required_fields]
            if missing_fields or unsolicited_fields:
                raise WrongFieldsError(missing_fields, unsolicited_fields)


    @staticmethod
    def verify_fields_update(request: dict) -> None:
        accepted_fields = ['name']

        for field in request.keys():
            if field not in accepted_fields:
                raise NotAcceptedFieldError(field, accepted_fields)
