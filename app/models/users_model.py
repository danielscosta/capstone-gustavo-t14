from app.configs.database import db
from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import validates
from dataclasses import dataclass
from werkzeug.security import generate_password_hash, check_password_hash
from app.exceptions import (
    InvalidFieldTypeError,
    WrongFieldsError,
    NotAcceptedFieldError,
    LoginError
)


@dataclass
class UserModel(db.Model):
    id: int
    name: str
    email: str

    __tablename__ = "users"

    id = Column(Integer, primary_key=True)
    name = Column(String(32), nullable=False)
    email = Column(String, unique=True, nullable=False)
    password_hash = Column(String, nullable=False)


    @property
    def password(self) -> AttributeError:
        raise AttributeError('you shouldn\'t be here')

    
    @password.setter
    def password(self, password_to_hash: str) -> None:
        if type(password_to_hash) != str:
            raise InvalidFieldTypeError('password', password_to_hash, 'string')

        self.password_hash = generate_password_hash(password_to_hash)

    
    def verify_password(self, password_to_compare: str) -> bool:    
        if not check_password_hash(self.password_hash, password_to_compare):
            raise LoginError


    @validates('name', 'email')
    def validate_fields(self, key: str, value: any) -> str:
        if type(value) != str:
            raise InvalidFieldTypeError(key, value, 'string')
        
        if key == 'name':
            return value.title()

        if key == 'email':
            return value.lower()


    @staticmethod
    def verify_fields_create(request: dict, required_fields: list) -> None:
        if list(request.keys()) != required_fields:
            missing_fields = [field for field in required_fields if field not in request.keys()]
            unsolicited_fields = [field for field in request.keys() if field not in required_fields]
            if missing_fields or unsolicited_fields:
                raise WrongFieldsError(missing_fields, unsolicited_fields)


    @staticmethod
    def verify_fields_update(request: dict) -> None:
        accepted_fields = ['name', 'password']

        for field in request.keys():
            if field not in accepted_fields:
                raise NotAcceptedFieldError(field, accepted_fields)
