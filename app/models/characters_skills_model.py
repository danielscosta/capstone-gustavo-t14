from sqlalchemy.orm import relationship
from app.configs.database import db
from sqlalchemy import Column, Integer, ForeignKey
from dataclasses import dataclass


characters_skills = db.Table('characters_skills',
    Column('id', Integer, primary_key=True),
    Column('value', Integer, default=0),
    Column('character_id', Integer, ForeignKey('characters.id', ondelete='cascade'), nullable=False),
    Column('skill_id', Integer, ForeignKey('skills.id'), nullable=False)
)


@dataclass
class CharactersSkills(db.Model):
    value: int
    skill_infos: str

    __table_args__ = {'extend_existing': True}
    __tablename__ = 'characters_skills'

    skill = relationship('SkillModel', overlaps='skills, value')
    skill_infos = relationship('SkillModel', overlaps='skill')
